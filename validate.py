#!/usr/bin/env python3

import sys, json, jsonschema

if len(sys.argv) != 3:
    print('ERROR: Wrong number of arguments')
    print('--')
    print('Usage:')
    print("\t" + sys.argv[0] + ' json_file json_schema_file')
    exit(1)

json_instance = json.load(open(sys.argv[1], 'r'))
json_schema = json.load(open(sys.argv[2], 'r'))

try:
    jsonschema.validate(json_instance, json_schema, jsonschema.Draft4Validator)
except jsonschema.exceptions.SchemaError as e:
    print('SchemaError: ', end='')
    print(e)
    exit(1)
except jsonschema.exceptions.ValidationError as e:
    print('ValidationError: ', end='')
    print(e)
    exit(1)
